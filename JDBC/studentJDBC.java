package jdbc;

import java.lang.reflect.Executable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class studentJDBC {

	public static void main(String[] args) throws Exception {
		
		
		studentJDBC jd = new studentJDBC();
		
		
//		jd.insert();
//		jd.update();
		jd.delete();
		jd.view();
	}
	
	private void delete() throws Exception {
		String url="jdbc:postgresql://localhost:5432/learnjdbc";
		String user="postgres";
		String password="monish";
		String query="delete from students where s_no=? ";
		
		Connection con = DriverManager.getConnection(url,user,password);
		PreparedStatement ps =con.prepareStatement(query);
		ps.setInt(1, 1);
		ps.execute();
	}

	private void update() throws Exception {

		String url="jdbc:postgresql://localhost:5432/learnjdbc";
		String user="postgres";
		String password="monish";
		String query="update students set name =? where s_no=?";
		
		Connection con = DriverManager.getConnection(url,user,password);
		PreparedStatement ps =con.prepareStatement(query);
		ps.setString(1, "manoj");
		ps.setInt(2,1);
		ps.executeUpdate();
		ps.close();
		
	}

	private void insert() throws SQLException {
		
		
		
		String url="jdbc:postgresql://localhost:5432/learnjdbc";
		String user="postgres";
		String password="monish";
		String query="insert into students values (?,?,?)";
		
		Connection con = DriverManager.getConnection(url,user,password);
		PreparedStatement ps =con.prepareStatement(query);
		ps.setInt(1, 4);
		ps.setString(2,"ragavan");
		ps.setInt(3, 29);
		ps.executeUpdate();
		ps.close();
	}

	public void view() throws Exception {
		String url="jdbc:postgresql://localhost:5432/learnjdbc";
		String user="postgres";
		String password="monish";
		String query="select * from students";
		Connection con = DriverManager.getConnection(url,user,password);
		Statement st=con.createStatement();
		ResultSet rs =st.executeQuery(query);
		
		while(rs.next()) {
			System.out.print(rs.getInt(1)+" ");
			System.out.print(rs.getString(2)+" ");
			System.out.print(rs.getInt(3)+" ");
			System.out.println();
		}
	}
}
