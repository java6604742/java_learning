package com.condition;

public class If {

	public static void main(String[] args) {
		
		int a =16448;
		int b=1628;
		int c =1628;
		
		if(a>b) {     // 1
			if(a>c) {
				System.out.println("a is bigger");
			}
			else if(c>a) {
				System.out.println(" c is big");
			}
			else {
				System.out.println("a and c equal");
			}
		}	//1
		
		else if (b>a) {   //2
			if(b>c) {
				System.out.println(" b is big");
			}
			
			else if(c>b) {
				System.out.println(" c is big");
			}
			else {
				System.out.println("b and c equal");
			}
			
		}              //2
		
		else {
			
			if(b>c) {
				System.out.println(" a and b equal");
			}
			else if(c>b) {
				System.out.println("c is big");
			}
			else {
				System.out.println("a , b and all are eqyal");
			}
			
		}
	}
}
