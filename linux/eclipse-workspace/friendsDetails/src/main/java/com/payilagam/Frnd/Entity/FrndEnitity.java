package com.payilagam.Frnd.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="frnd_details")
public class FrndEnitity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int f_No;
	private String name;
	private String gender;
	private int age;
	public int getF_No() {
		return f_No;
	}
	public void setF_No(int f_No) {
		this.f_No = f_No;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	} 
}
