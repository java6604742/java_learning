package com.payilagam.Frnd.controller;

import org.slf4j.helpers.Reporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.payilagam.Frnd.Entity.FrndEnitity;
import com.payilagam.Frnd.repository.Repository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;



@Controller
public class FrndController {

	@Autowired
	Repository repo;
	
	@GetMapping("index")
	public ModelAndView homePage() {
		ModelAndView mv = new ModelAndView("FrndDetials");
		mv.addObject("frnd_obj",repo.findAll());
		return mv;
	}
	
	@GetMapping("/add")
	public ModelAndView addPage(FrndEnitity enty) {
		ModelAndView mv = new ModelAndView("addNew");
	    mv.addObject("addObj",enty);
		return mv;
	}
	
	@PostMapping("/add")
	public String savePage(FrndEnitity enty) {
		
		repo.save(enty);
		return "redirect:index";
	}
	
	@GetMapping("/update/{id}")
	public ModelAndView  updateFrnd(@PathVariable int id) {
		
		ModelAndView mv = new ModelAndView("updateFrnd");
		mv.addObject("update_frnd",repo.findById(id).get());
		return mv;
	}
	
	 @PostMapping("/update/{id}")
	   public String updateIndex(@PathVariable int id ,FrndEnitity enty) {
	  
	       enty.setF_No(id);
	       repo.save(enty);
	       return "redirect:/index";
	   }
	 
	 @GetMapping("/delete/{id}")
	 public String deleteFrnd(@PathVariable int id) {
		 repo.deleteById(id);
		 return  "redirect:/index";
		 
	 }
	 
	
}
