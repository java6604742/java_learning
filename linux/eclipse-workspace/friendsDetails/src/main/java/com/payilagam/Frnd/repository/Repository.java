package com.payilagam.Frnd.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.payilagam.Frnd.Entity.FrndEnitity;

public interface Repository extends JpaRepository<FrndEnitity, Integer> {

}
