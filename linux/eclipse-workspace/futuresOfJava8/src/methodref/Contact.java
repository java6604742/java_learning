package methodref;

@FunctionalInterface
public interface Contact {
 
	public void display(int no);
	
}
