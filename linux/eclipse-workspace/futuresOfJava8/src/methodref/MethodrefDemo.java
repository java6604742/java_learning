package methodref;

public class MethodrefDemo {

	public void show(int no) {
		System.out.println(no);
	}
	
	public MethodrefDemo(int no) {
		System.out.println("construct ----> " +no);
	}
	public static void main(String[] args) {
//		Contact c= System.out::println;     
		
//		MethodrefDemo md = new MethodrefDemo();
//		Contact c =md::show;
		
//	Contact c= new MethodrefDemo()::show;
		
		Contact c=MethodrefDemo::new;
		c.display(22);
		
		
		
	}
}
