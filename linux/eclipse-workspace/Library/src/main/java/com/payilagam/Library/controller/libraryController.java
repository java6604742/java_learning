package com.payilagam.Library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.support.Repositories;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.payilagam.Library.Entity.libraryEntity;
import com.payilagam.Library.Repository.Repository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;



@Controller
public class libraryController {

	@Autowired
	Repository repo;
	
	
	@GetMapping("index")
	public ModelAndView method() {
		
		ModelAndView mv = new ModelAndView("library");
		mv.addObject("lib",repo.findAll());
		return mv;
		
	}
	
	@GetMapping("/add")
	public ModelAndView getMethodName(libraryEntity le) {
		
		ModelAndView mv = new ModelAndView("BookStore");
	     mv.addObject("bookadd",le);
		return mv;
	}
	
	
	@PostMapping("/add")
	public String postMethodName(libraryEntity Postle) {
		
		repo.save(Postle);
		return "redirect:index";
	}
	
   @GetMapping("/update/{id}")
   public ModelAndView updating(@PathVariable int id) {
	   
	   ModelAndView mv = new ModelAndView("update");
	   mv.addObject("up_data",repo.findById(id).get());
       return mv;
   }
   
   @PostMapping("/update/{id}")
   public String UpdatedData(@PathVariable int id,libraryEntity enty) {
       enty.setS_no(id);
       repo.save(enty);
	   return "redirect:/index";
   }
   
   @GetMapping("/delete/{id}")
   public String deleteData(@PathVariable int id) {
	   repo.deleteById(id);
	   return "redirect:/index";
   }
   
   
   
	
	
}
