package com.payilagam.Library.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.payilagam.Library.Entity.libraryEntity;

public interface Repository extends JpaRepository<libraryEntity, Integer>{

	
}
