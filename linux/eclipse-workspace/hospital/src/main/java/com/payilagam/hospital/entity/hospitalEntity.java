package com.payilagam.hospital.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@Entity
@Table(name ="hospital_mang")
public class hospitalEntity {

	@Id
@GeneratedValue(strategy = GenerationType.SEQUENCE)
private int P_no;
private String P_name;
private int age;
private String Gender;
public int getP_no() {
	return P_no;
}
public void setP_no(int p_no) {
	P_no = p_no;
}
public String getP_name() {
	return P_name;
}
public void setP_name(String p_name) {
	P_name = p_name;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public String getGender() {
	return Gender;
}
public void setGender(String gender) {
	Gender = gender;
}
}
