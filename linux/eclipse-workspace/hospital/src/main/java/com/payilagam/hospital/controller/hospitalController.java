package com.payilagam.hospital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.payilagam.hospital.entity.hospitalEntity;
import com.payilagam.hospital.repository.hospitalRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@Controller
public class hospitalController {

	@Autowired
	hospitalRepository repo;
	
	@GetMapping("index")
	public ModelAndView indexPage() {
		
		ModelAndView mv = new ModelAndView("home");
		mv.addObject("data",repo.findAll());
		return mv;
		
	}
	
	@GetMapping("/add")
	public ModelAndView addPatient(hospitalEntity enty) {
				ModelAndView mv = new ModelAndView("addPatient");
				mv.addObject("patent",enty);
		       return mv ;
	}
	
	@PostMapping("/add")
	public String addedData(hospitalEntity enty) {
		repo.save(enty);
		return "redirect:/index";
	}
	
	
}

