package com.payilagam.hotel.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.payilagam.hotel.Entity.hotelEntity;

public interface Repository extends JpaRepository<hotelEntity, Integer>{

}
