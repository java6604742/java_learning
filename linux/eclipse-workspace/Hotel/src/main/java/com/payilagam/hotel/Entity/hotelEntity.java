package com.payilagam.hotel.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity

@Table(name="hotelManage")
public class hotelEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int meno_no;
	private String dishName;
	private int price;
	public int getMeno_no() {
		return meno_no;
	}
	public void setMeno_no(int meno_no) {
		this.meno_no = meno_no;
	}
	public String getDishName() {
		return dishName;
	}
	public void setDishName(String dishName) {
		this.dishName = dishName;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	 
	
}
