package com.payilagam.hotel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import com.payilagam.Library.Entity.libraryEntity;

import com.payilagam.hotel.Entity.hotelEntity;
import com.payilagam.hotel.repository.Repository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
public class HotelController {

	@Autowired
	Repository repo;
	
	@GetMapping("index")
	public ModelAndView hotelPage() {
		 ModelAndView mv = new ModelAndView("hotel");
		 mv.addObject("h_obj",repo.findAll());
		 return mv;
	}
	
	@GetMapping("/add")
	public ModelAndView add_dish(hotelEntity le) {
		ModelAndView mv = new ModelAndView("add_Dish");
		 mv.addObject("Add_obj",le);
		 return mv;
	}
	
	@PostMapping("/add")
	public String added_data(hotelEntity le) {
		
		repo.save(le);
		return "redirect:/index";
	}
	
	@GetMapping("/update/{id}")
	public ModelAndView update_data(@PathVariable int id) {
		ModelAndView mv = new ModelAndView("update_hotel");
		mv.addObject("update_ob",repo.findById(id).get());
		return mv;
		
	}
	
	@PostMapping("/update/{id}")
	public String submit_updata(@PathVariable int id, hotelEntity enty) {
	    enty.setMeno_no(id);
	    repo.save(enty);
	    return "redirect:/index";
	}
	
	@GetMapping("delete/{id}")
	public String getMethodName(@PathVariable int id) {
		repo.deleteById(id);
		return  "redirect:/index";
	}
	
	

}
