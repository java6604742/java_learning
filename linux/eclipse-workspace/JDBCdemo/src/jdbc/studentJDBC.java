package jdbc;

import java.lang.reflect.Executable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class studentJDBC {

	public static void main(String[] args) throws Exception {
		
		
		studentJDBC jd = new studentJDBC();
//		jd.create() ;
		
//		jd.update();
		
		jd.delete();
		jd.view();
	}

	private void delete() throws Exception{
		
		String url="jdbc:postgresql://localhost:5432/learnjdbc";
		String user="postgres";
		String password="monish";
		String qurry="DELETE FROM students WHERE s_no=?";
		
		Connection con =DriverManager.getConnection(url,user,password);
		PreparedStatement ps = con.prepareStatement(qurry);
		ps.setInt(1, 2);
		ps.execute();
	}

	private void update() throws Exception{
		
		String url="jdbc:postgresql://localhost:5432/learnjdbc";
		String user="postgres";
		String password="monish";
		String qurry="update students set age =? where s_no=?";
		
		Connection con =DriverManager.getConnection(url,user,password);
		PreparedStatement ps = con.prepareStatement(qurry);
		ps.setInt(1, 6);
	
		ps.setInt(2, 2);
		
		ps.execute();
		ps.close();
	}

	private void create() throws Exception{
		String url="jdbc:postgresql://localhost:5432/learnjdbc";
		String user="postgres";
		String password="monish";
		String qurry="insert into students values (?,?,?)";
		
		Connection con =DriverManager.getConnection(url,user,password);
		PreparedStatement ps = con.prepareStatement(qurry);
		ps.setInt(1, 6);
		ps.setString(2,"monish" );
		ps.setInt(3, 2);
		
		ps.execute();
		ps.close();
		
	}

	private void view() throws SQLException {
		
		String url="jdbc:postgresql://localhost:5432/learnjdbc";
		String user="postgres";
		String password="monish";
		String qurry="select * from students";
		
		Connection con =DriverManager.getConnection(url,user,password);
		Statement sm =con.createStatement();
		ResultSet rs = sm.executeQuery(qurry);
		
		
		while(rs.next()) {
			System.out.print(rs.getInt(1));
			System.out.println(rs.getString(2));
			System.out.println(rs.getInt(3));
		}
	}
	
}
