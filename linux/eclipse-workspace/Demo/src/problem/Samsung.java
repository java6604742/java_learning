package problem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Samsung implements Comparable {

	int price, ram, mp;

	public Samsung(int price, int ram, int mp) {
		this.price = price;
		this.ram = ram;
		this.mp = mp;
	}

	public static void main(String[] args) {
		Samsung s1 = new Samsung(100000, 6, 12);
		Samsung s2 = new Samsung(100000, 8, 16);
		int result = s1.compareTo(s2);
		System.out.println(result);
		if (result > 0) {
			System.out.println("s1 price is higher");
		} else if (result < 0) {
			System.out.println("s2 price is higher");
		} else {
			System.out.println("s1 and s2 price are equal");
		}
		
		
		
		
		
		
		ArrayList names = new ArrayList();
		names.add("Monish");
		names.add("Moorthy");
		names.add("selva");
		names.add("sarathkumar");
		names.add("prabakaran");
		System.out.println(names);
		
		ComparatorDemo cd = new ComparatorDemo();
		Collections.sort(names,cd);
		
		
		System.out.println(names);
		
	}

	
	
	
	
	
	@Override // up casting
	public int compareTo(Object obj) {
		Samsung s2 = (Samsung) obj; // down casting
		if (this.price > s2.price) // s1.price>s2.price
		{
			return +75671;
		} else if (s2.price > this.price) {
			return -21342;
		}
		return 0;
	}
}