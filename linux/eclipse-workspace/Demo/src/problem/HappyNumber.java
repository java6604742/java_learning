package problem;


public class HappyNumber {
	public boolean isHappy(int n) {
        if (n<=4) {
            if (n==1) return true;
            return false;
        }

        int p = 0;
        while(n>0) {
            int p1 = n%10;
            n = n/10;
            p += p1*p1;
        }
        return isHappy(p);
    }
	
	public static void main(String[] args) {
		HappyNumber s = new HappyNumber();
		
		System.out.println(s.isHappy(49));
	}
}


