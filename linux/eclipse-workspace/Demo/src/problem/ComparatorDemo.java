package problem;

import java.util.Comparator;

class ComparatorDemo implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		String s1 = (String)o1;
		String s2 = (String)o2;
		int result = s1.compareTo(s2);
		if(s1.length()>s2.length()) {
			return -123;
		}else if(s2.length()>s1.length()) {
			return +213;
		}
		return 0;
	}

}
