package Typecasting;

import problem.second;

public class Enginnering extends Student {

	public static void main(String[] args) {
		
		Enginnering eng = new Enginnering();  
		eng.study();
		eng.project();
		Student s1= (Student)eng;   // upcasting child to parent = upcating
		s1.study();                //  it can access only student methods like dynamic binding
	
	}
	
	public void project() {
		System.out.println("doing project");
	}
	public void study() {
		System.out.println("engineer studing");
	}
}
