package Typecasting;

public class Student {
	public static void main(String[] args) {
		
		//Student s = new Student();
		
		Student ss = new Enginnering();     // before downcasting we have to make dynamic binding
		
	    Enginnering eng1 = (Enginnering)ss; // down casting  =parent to child 
	            	eng1.project();        // now this object is enginner we can access 
	            	eng1.study();         //  engineering methods
		
	}
	public void study() {
		System.out.println("student is studying");
	}
	

}
