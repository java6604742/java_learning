package collection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Map2 {
 
    public static void main(String[] args) {
        
        HashMap hs = new HashMap();
        hs.put("idli", 40);
        hs.put("dosa", 20);
        hs.put("pongal", 39);
        hs.put("puttu", 29);
        
        System.out.println(hs);
        System.out.println(hs.get("idli")); 
        // System.out.println(hs.remove("39"));
        hs.replace("pongal", 40);
        System.out.println(hs);
        System.out.println(hs.containsValue(29));   // it checks the value here or not 
        System.out.println(hs.containsKey("dosa")); // it checks the key here or not
    
        System.out.println(hs.entrySet());  
        System.out.println(hs.keySet());
        System.out.println(hs.values());
    
        Set  entrySet = hs.entrySet();
        
        Iterator iterator = entrySet.iterator();
        
        while (iterator.hasNext()) {
            Map.Entry me = (Entry) iterator.next();
            System.out.println(me.getKey());
            
            System.out.println((int)me.getValue()+5);
        }
        System.out.println(hs);
    }
}
