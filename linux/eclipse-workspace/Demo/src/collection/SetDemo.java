package collection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeSet;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.plaf.basic.BasicTreeUI.TreeCancelEditingAction;


public class SetDemo {

	
	public static void main(String[] args) {
		
		ArrayList ar = new ArrayList();
		ar.add("selvakumar");
		ar.add("monish");
		
		
		HashSet hs = new HashSet(ar);
		hs.add("monish");
		hs.add("selva");
		hs.add("sarath anna");
		hs.add("eswar");
		hs.add("selva");
		System.out.println(hs);
		
		
		TreeSet ts = new TreeSet(hs);
		System.out.println(ts);
		
		
		
		
	}
}

