package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Jdbc {

	public static void main(String[] args) throws Exception {
		
		Jdbc j = new Jdbc();
//		j.create();
//		j.select();
//		j.update();
		j.delete();
	}
	
	private void delete() throws Exception{
		
		
		    String url = "jdbc:postgresql://localhost:5432/learnjdbc";
		    String userName = "postgres";
		    String password = "monish";
		    String query = "DELETE FROM employee WHERE id=?";
		    
		    Connection con = DriverManager.getConnection(url, userName, password);
		         PreparedStatement pst = con.prepareStatement(query );

		      
		        pst.setInt(1, 1)  ;
		         pst.executeUpdate();

		      
		    }
		

	

	private void update() throws Exception{
		String url ="jdbc:postgresql://localhost:5432/learnjdbc";
		String userNAme="postgres";
		String password="monish";
		String qurry="update employee set salary =? where id=?";
		
		Connection con =DriverManager.getConnection(url,userNAme,password);
		PreparedStatement pst =con.prepareStatement(qurry);
		pst.setInt(1, 100);
		pst.setInt(2,2);
		
		int row=pst.executeUpdate();
//		System.out.print(false);
		con.close();
		
	}

	public void select() throws SQLException {
	String url ="jdbc:postgresql://localhost:5432/learnjdbc";
	String userNAme="postgres";
	String password="monish";
	String qurry="select * from employee";
	
	Connection con =DriverManager.getConnection(url,userNAme,password);
	Statement st = con.createStatement();
	ResultSet rs = st.executeQuery(qurry);
	
	while(rs.next())
	{
		System.out.println(rs.getInt(1));
		System.out.println(rs.getString(2));
		System.out.println(rs.getInt(3));
	}
	con.close();
}
	
	public void create() throws SQLException 
	{
		
		String url ="jdbc:postgresql://localhost:5432/learnjdbc";
		String userNAme="postgres";
		String password="monish";
		String qurry="insert into employee values (?,?,?)";
		
		Connection con =DriverManager.getConnection(url,userNAme,password);
		PreparedStatement pst =con.prepareStatement(qurry);
		pst.setInt(1, 2);
		pst.setString(2,"manoj");
		pst.setInt(3,40000);
		
		int row=pst.executeUpdate();
//		System.out.print(false);
		con.close();
	}
	
	
}
