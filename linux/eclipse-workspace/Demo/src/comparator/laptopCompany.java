package comparator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class laptopCompany implements Comparator{
	int price;
	String brand;
	int ram;

	public laptopCompany(String brand, int ram , int price) {
		this.price=price;
		this.ram=ram;
		this.brand=brand;
	}

	public laptopCompany() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		ArrayList al = new ArrayList();
		al.add(new laptopCompany("lenovo",12,20000));
		al.add(new laptopCompany("dell",12,3000));
		al.add(new laptopCompany("asus",14,3000));
		
		laptopCompany c = new laptopCompany();
		Collections.sort(al,c);
		System.out.println(al);
	}

	@Override
	public int compare(Object o1, Object o2) {
		
		laptopCompany l1= (laptopCompany) o1;
		laptopCompany l2= (laptopCompany) o2;
		
		if(l2.ram>l1.ram)
			return -1;
			else
		return 1;
	}
	@Override
	public String toString() {
		
		return ""+price+" "+brand+" "+ram;
	}
}
