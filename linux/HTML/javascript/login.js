const form=document.querySelector("#form")
const username=document.querySelector("#username")
const email=document.querySelector("#email")
const password=document.querySelector("#password")
const cpassword=document.querySelector("#cpassword")

form.addEventListener('submit',(obj)=>{
    if(validateInputs()==false)
        obj.preventDefault();
})

function validateInputs(){
   const username_value= username.value.trim()
   const email_value= email.value.trim()
   const password_value= password.value.trim()
   const cpassword_value= cpassword.value.trim()


   if(username_value==="")
   {
    success=false
    setError(username,'User Name Required')
   }
   else{
    setSuccess(username)
   }

   if(email_value==="")
   {
    success=false
    setError(email,'Email Id Required')
   }
   else if(validateEmail(email_value)==false)
   {
    success=false
    setError(email,'Invalid Email Id')
   }
   else{
    setSuccess(email)
   }

   

  
   if(password_value==="")
   {
    success=false
    setError(password,'Password Required')
   }
   else if(password_value.length<8)
   {
    success=false
    setError(password,'Password must have atleast 8 characters long')
   }
   else{
    setSuccess(password)
   }


   if(cpassword_value==="")
   {
    success=false
    setError(password,'Password Required')
   }
   else if(password_value!==password_value)
   {
    success=false
    setError(password,'password must be same')
   }
   else{
    setSuccess(cpassword)
   }

   return success

}

function setError(element,message)
{
    let input_group= element.parentElement
    let error_element=input_group.querySelector('.error')

    error_element.innerHTML=message
    input_group.classList.add('error')
}

function setSuccess(element)
{
    let input_group= element.parentElement
    let error_element=input_group.querySelector('.error')

    error_element.innerHTML=""
    input_group.classList.add('success')
    input_group.classList.remove('error')
}
function validateEmail(ev) {
    return String(ev).toLowerCase().match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/);
  }