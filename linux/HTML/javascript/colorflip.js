let text = document.getElementById("color");
    let bg = document.getElementById("back_ground");
    let btn = document.getElementById("btn");

    let hex = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F'];

    btn.addEventListener('click', changebg);

    function changebg() {
      let colorhex = '#';

      for (let i = 0; i < 6; i++) {
        colorhex += random();
      }

      text.innerHTML = colorhex;
      bg.style.backgroundColor = colorhex;
    }

    function random() {
      let randomNo = Math.floor(Math.random() * 16);
      return hex[randomNo];
    }