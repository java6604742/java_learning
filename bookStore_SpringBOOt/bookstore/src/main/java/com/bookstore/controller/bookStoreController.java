package com.bookstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bookstore.Enity.bookStore_Enitity;
import com.bookstore.repository.BookstoreRepository;


@Controller
public class bookStoreController {

	@Autowired
	BookstoreRepository repo;
	
	@GetMapping("book")
	public String home() {
		return "index";
	}
	
	@GetMapping("add")
	public String getMethodName(bookStore_Enitity enty) {
		
		repo.save(enty);
	
		return "index";
	}
	
}
