
package com.bookstore.Enity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="contact_details")
public class bookStore_Enitity {

@Id
@GeneratedValue(strategy = GenerationType.SEQUENCE)
  private int sNo;
  private String Name;
  private String mobileNo;
public int getsNo() {
	return sNo;
}
public void setsNo(int sNo) {
	this.sNo = sNo;
}
public String getName() {
	return Name;
}
public void setName(String name) {
	Name = name;
}
public String getMobileNo() {
	return mobileNo;
}
public void setMobileNo(String mobileNo) {
	this.mobileNo = mobileNo;
}
}